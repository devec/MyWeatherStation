'''
    Author: Alexander Gerber
    E-Mail: alex.gerber90@gmail.com
    Python Version: 3.5
    Description: class for SQL connection
    today support online python inbuild sqlite3
    Singleton pattern
'''

import sqlite3
import time


class SQLHandler:

    class __impl:
        def __init__(self, database__, measurements__):
            self.database = database__
            self.measurements = measurements__
            try:
                self.connection = sqlite3.connect(database__)
            except Exception as e:
                print("Cannot contact the database. reason: ", e)

            self.cursor = self.connection.cursor()
            for measure in self.measurements:
                self.checkTable(measure, self.cursor)
            self.connection.commit()

        def __del__(self):
            self.connection.close()

        def checkTable(self, table, db_cursor):
            sql_command = """
                                    CREATE TABLE IF NOT EXISTS """ + table + """ ( 
                                    date TIME,
                                    """ + table +""" FLOAT);
                                    """
            try:
                db_cursor.execute(sql_command)
            except Exception as e:
                print("Cannot execute SQL-command. reason: ", e)

        def insertMeasuredValueDatabase(self, measuredValue, tableName):

            sql_command = """INSERT INTO """ + tableName + """ (date, """ + tableName + """)
                            VALUES ( '""" + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time())) + """', """ + str(measuredValue) + """);"""

            try:
                self.cursor.execute(sql_command)
            except Exception as e:
                print("Cannot execute SQL-command. reason: ", e)
            try:
                self.connection.commit()
            except Exception as e:
                print("Cannot commit to the database. reason: ", e)


    __instance = None

    def __init__(self, database_, measurements_):
        if SQLHandler.__instance is None:
            SQLHandler.__instance = SQLHandler.__impl(database_, measurements_)
        self.__dict__['_Singleton__instance'] = SQLHandler.__instance

    def __getattr__(self, attr):
        return getattr(self.__instance, attr)

    def __setattr__(self, attr, value):
        return setattr(self.__instance, attr, value)
