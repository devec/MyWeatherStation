'''
    Author: Alexander Gerber
    E-Mail: alex.gerber90@gmail.com
    Python Version: 3.5
    Description: Write data to csv-file or to SQL-database
    Singleton pattern
'''

import csv
import time
import os
import SQLHandler


class Writer:

    class __impl:

        def __init__(self,sensors__):
            self.sensors = sensors__
            self.sensorpath = os.getcwd() + '/sensordata'
            if not os.path.exists(self.sensorpath):
                os.makedirs(self.sensorpath)
            self.timefile = time.time()

        def checkTime(self):
            if(time.time() > (self.timefile + 3600.0)):
                self.timefile = time.time()

        def addDataCsv(self, filename, sensordata):
            with open(self.sensorpath + '/' + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(self.timefile)) + ' ' + filename, 'a') as csvfile:
                spamwriter = csv.writer(csvfile, delimiter=' ', quotechar='|', quoting=csv.QUOTE_MINIMAL)
                spamwriter.writerow([time.strftime("%M:%S", time.localtime(time.time()-self.timefile))] + [str(sensordata)])

        def writeCsvAndAddSqlValue(self):
            self.checkTime()
            for sensor in self.sensors:
                temp_sensor_data = sensor.getSensorData()
                self.measurements = sensor.getMeasurements()
                self.sqlHandler = SQLHandler.SQLHandler("sensors.db", self.measurements)
                for idx, measurement in enumerate(self.measurements):
                    self.addDataCsv(measurement + '.csv', temp_sensor_data[idx])
                    self.sqlHandler.insertMeasuredValueDatabase(temp_sensor_data[idx], measurement)

        def addSqlValue(self):
            self.checkTime()
            for sensor in self.sensors:
                temp_sensor_data = sensor.getSensorData()
                self.measurements = sensor.getMeasurements()
                self.sqlHandler = SQLHandler.SQLHandler("sensors.db", self.measurements)
                for idx, measurement in enumerate(self.measurements):
                    self.sqlHandler.insertMeasuredValueDatabase(temp_sensor_data[idx], measurement)

        def writeCsv(self):
            self.checkTime()
            for sensor in self.sensors:
                temp_sensor_data = sensor.getSensorData()
                self.measurements = sensor.getMeasurements()
                for idx, measurement in enumerate(self.measurements):
                    self.addDataCsv(measurement + '.csv', temp_sensor_data[idx])

    __instance = None

    def __init__(self, sensor_):
        if Writer.__instance is None:
            Writer.__instance = Writer.__impl(sensor_)

        self.__dict__['_Singleton__instance'] = Writer.__instance

    def __getattr__(self, attr):
        return getattr(self.__instance, attr)

    def __setattr__(self, attr, value):
        return setattr(self.__instance, attr, value)
