'''
    Author: Alexander Gerber
    E-Mail: alex.gerber90@gmail.com
    Python Version: 3.5
    Description: Select data from SQL-database and create plot with matplotlib.
    Singleton pattern
'''

import sqlite3
import os
import matplotlib.pyplot as plt
import numpy

class Plotter:
    def __init__(self, table_, database_):
        self.table = table_
        self.database = database_
        self.imagePath = os.getcwd() + '/static'
        if not os.path.exists(self.imagePath):
            os.makedirs(self.imagePath)

        try:
            self.connection = sqlite3.connect(self.database)
        except Exception as e:
            print("Cannot contact the database. reason: ", e)

        self.cursor = self.connection.cursor()
        for table in self.table:
            self.checkTable(table, self.cursor)
        self.connection.commit()

    def __del__(self):
        self.connection.close()

    def checkTable(self, table, databaseCursor):
        sql_command = """
                            CREATE TABLE IF NOT EXISTS """ + table + """ ( 
                            date TIME,
                            """ + table +""" FLOAT);
                            """
        try:
            databaseCursor.execute(sql_command)
        except Exception as e:
            print("Cannot execute SQL-command. reason: ", e)

    def selectDataFromTable(self, table, measuredValue, databaseCursor):
        try:
            databaseCursor.execute("SELECT date FROM " + table)
        except Exception as e:
            print("Cannot execute SQL-command. reason: ", e)
        date__ = databaseCursor.fetchall()
        try:
            databaseCursor.execute("SELECT " + measuredValue + " FROM " + table)
        except Exception as e:
            print("Cannot execute SQL-command. reason: ", e)
        data__ = databaseCursor.fetchall()

        return date__, data__

    def createPlot(self, measurements, measurementsMaximumVariance):
        for idxMeasurement, measurement in enumerate(measurements):
            dateMeasurement, dataMeasurement = self.selectDataFromTable(measurement, measurement, self.cursor)
            # with explizit  time today unsoppurted
            # create list, which big enuogh
            resultidx = list([ None ] * len(dataMeasurement))
            resultdata = list([None] * len(dataMeasurement))
            idx_add = 0
            for idx_r, r in enumerate(dataMeasurement):
                if ((r[0] > (numpy.mean(dataMeasurement) + measurementsMaximumVariance[idxMeasurement])) or (r[0] < (numpy.mean(dataMeasurement) - measurementsMaximumVariance[idxMeasurement]))):
                    idx_add = idx_add + 1
                    # skip adding, because bad measurement
                else:
                    resultidx[idx_r - idx_add] = idx_r
                    resultdata[idx_r -idx_add] = r[0]
            plt.plot(resultidx, resultdata) # todo cut None types
            plt.savefig('static/' + measurement + '.png')
            plt.clf()


