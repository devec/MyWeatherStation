'''
    Author: Alexander Gerber
    E-Mail: alex.gerber90@gmail.com
    Python Version: 3.5
    Description: class for DHT 11/22 Sensor
    Please change Sensor and Pin in BackgroundProcess.py .
    Singleton pattern
'''

import Adafruit_DHT
import time

class SensorDHT:

    class __impl:
        def __init__(self,DHT_SENSOR__, DHT_PIN__, sensors__):
            self.DHT_SENSOR = DHT_SENSOR__
            self.DHT_PIN = DHT_PIN__
            self.sensors = sensors__
        def getMeasurements(self):
            return self.sensors

        def getSensorData(self):
            temperature = None
            humidity = None
            while temperature == None:
                try:
                    humidity, temperature = Adafruit_DHT.read_retry(self.DHT_SENSOR, self.DHT_PIN)
                except Exception as e:
                    print("The readout of the sensor has failed. reason: ", e)
                    temperature = "The readout of the sensor has failed reason: " + str(e)
                if temperature == None:
                    time.sleep(1.5)
            if(('temperature' in self.sensors) and ('humidity' in self.sensors)):
                return [temperature, humidity]
            elif('temperature' in self.sensors):
                return [temperature]
            else:
                return [humidity]

    __instance = None

    def __init__(self,DHT_SENSOR_, DHT_PIN_, sensors_):
        if SensorDHT.__instance is None:
            SensorDHT.__instance = SensorDHT.__impl(DHT_SENSOR_, DHT_PIN_, sensors_)
        self.__dict__['_Singleton__instance'] = SensorDHT.__instance

    def __getattr__(self, attr):
        return getattr(self.__instance, attr)

    def __setattr__(self, attr, value):
        return setattr(self.__instance, attr, value)