'''
    Author: Alexander Gerber
    E-Mail: alex.gerber90@gmail.com
    Python Version: 3.5
    Description: Flask webserver
    Singleton pattern
'''

from flask import Flask
from flask import render_template


import time
import Plotter


class HttpWebserver:

    class __impl:
        def __init__(self):

            app = Flask(__name__)

            run = True
            # Run the webserver
            # start the process with load ther main page
            # in futher version a goal is a better interaction
            @app.route('/')
            @app.route('/index')
            def index():
                try:
                    plotter_inst = Plotter.Plotter(['temperature', 'humidity'],'sensors.db')
                    plotter_inst.createPlot(['temperature', 'humidity'], [4.0, 20.0])
                except Exception as e:
                    print("Cannot create the plot. reason: ", e)
                return render_template('index.html')
            app.run(debug=True, host='127.0.0.1')

    __instance = None

    def __init__(self):
        if HttpWebserver.__instance is None:
            HttpWebserver.__instance = HttpWebserver.__impl()
        self.__dict__['_Singleton__instance'] = HttpWebserver.__instance

    def __getattr__(self, attr):
        return getattr(self.__instance, attr)

    def __setattr__(self, attr, value):
        return setattr(self.__instance, attr, value)
