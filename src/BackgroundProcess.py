# !/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
    Author: Alexander Gerber
    E-Mail: alex.gerber90@gmail.com
    Python Version: 3.5
    Description:  This is main file for the pickup data from Sensors
    and write it to SQL-database or csv-file.
'''

import Adafruit_DHT
import SensorDHT
import Writer
import time


''' Define of pin and sensors'''
DHT_PIN = 23  # GPIO nr
DHT_SENSOR = Adafruit_DHT.DHT11 # DHT11 and DHT22 supported

'''Define time to stop the programm'''
stoptime = 1200.0
'''Define medium to write: SQL, CSV or BOTH'''
medium = "SQL"

SensorDHT11 = SensorDHT.SensorDHT(DHT_SENSOR, DHT_PIN, ['temperature', 'humidity'])
Writer_inst = Writer.Writer([SensorDHT11])
akt = time.time()
if(medium == "SQL"):
    while ((akt + stoptime) > time.time()):
        Writer_inst.addSqlValue()
elif(medium == "CSV"):
    while ((akt + stoptime) > time.time()):
        Writer_inst.addDataCsv()
elif(medium == "BOTH"):
    while ((akt + stoptime) > time.time()):
        Writer_inst.writeCsvAndAddSqlValue()